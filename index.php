<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
define('ROOT_DIR', dirname(__FILE__));
define('ROOT_URL', substr($_SERVER['PHP_SELF'], 0, - (strlen($_SERVER['SCRIPT_FILENAME']) - strlen(ROOT_DIR))));

$lang = 'es';

if (isset($_GET['lang'])) {
    $lang = $_GET['lang'];
    setcookie('lang', $lang, strtotime('+30 days'), ROOT_URL);
} else if (isset($_COOKIE['lang'])) {
    $lang = $_COOKIE['lang'];
}

require './config/i18n/' . $lang . '.php';
require_once './config/routes.php';

$method = $_SERVER['REQUEST_METHOD'];
$url = empty($_GET['url']) ? '' : $_GET['url'];
$route = strtolower($method . '/' . $url);
if (array_key_exists($route, $routes)) {
    include_once $routes[$route];
} else {
    include_once 'views/error404.php';
}
