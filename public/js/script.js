//@prepros-prepend scrollspy.js

var wow = new WOW();
wow.init();

var navBar = $(".navbar-fixed-top");
var modalProyectos;
var tituloProyectos;
var modalImagen;
var btnUp;
var formulario;
$(function () {
    $('nav').on('activate.bs.scrollspy', scrollbar);
    $(".scrolll-animate").on("click", scrollAnimate);
    modalProyectos = $("#myModal");
    modalImagen = $("#modal-imagen");
    tituloProyectos = $("#myModalLabel");
    btnUp = $("#btn-up");
    $(".list-proyectos a").click(showModal);
    $(window).scroll(scrollar);
    scrollar();
    formulario = $("#contacto");
    formulario.submit(validar);
    reloadWow();
});

var reloadWow = function () {
    WOW.prototype.addBox = function (element) {
        this.boxes.push(element);
    };
    $('.wow').on('scrollSpy:exit', function () {
        $(this).css({
            'visibility': 'hidden',
            'animation-name': 'none'
        }).removeClass('animated');
        wow.addBox(this);
    }).scrollSpy();
};

var formLoaded = true;
var scrollbar = function (e) {
    var a = $("a[href^='#']", e.target).attr("href");
    if (formLoaded && (a === "#clientes" || a === "#contacto")) {
        formLoaded = false;
        loadForm();
    }
    history.replaceState({}, "", a);
    //virtualpage
};
var initMap = null;
var loadForm = function () {
    initMap = function () {
        var uluru = {
            lat: 21.146760
            , lng: -86.83928
        };
        var map = new google.maps.Map(document.getElementById('mapa'), {
            zoom: 15
            , center: uluru
            , scrollwheel: false
            , styles: [
                {
                    "elementType": "geometry"
                    , "stylers": [
                        {
                            "color": "#212121"
                        }
                    ]
                }
                , {
                    "elementType": "labels.icon"
                    , "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                }
                , {
                    "elementType": "labels.text.fill"
                    , "stylers": [
                        {
                            "color": "#757575"
                        }
                    ]
                }
                , {
                    "elementType": "labels.text.stroke"
                    , "stylers": [
                        {
                            "color": "#212121"
                        }
                    ]
                }
                , {
                    "featureType": "administrative"
                    , "elementType": "geometry"
                    , "stylers": [
                        {
                            "color": "#757575"
                        }
                    ]
                }
                , {
                    "featureType": "administrative.country"
                    , "elementType": "labels.text.fill"
                    , "stylers": [
                        {
                            "color": "#9e9e9e"
                        }
                    ]
                }
                , {
                    "featureType": "administrative.land_parcel"
                    , "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                }
                , {
                    "featureType": "administrative.locality"
                    , "elementType": "labels.text.fill"
                    , "stylers": [
                        {
                            "color": "#bdbdbd"
                        }
                    ]
                }
                , {
                    "featureType": "poi"
                    , "elementType": "labels.text.fill"
                    , "stylers": [
                        {
                            "color": "#757575"
                        }
                    ]
                }
                , {
                    "featureType": "poi.park"
                    , "elementType": "geometry"
                    , "stylers": [
                        {
                            "color": "#181818"
                        }
                    ]
                }
                , {
                    "featureType": "poi.park"
                    , "elementType": "labels.text.fill"
                    , "stylers": [
                        {
                            "color": "#616161"
                        }
                    ]
                }
                , {
                    "featureType": "poi.park"
                    , "elementType": "labels.text.stroke"
                    , "stylers": [
                        {
                            "color": "#1b1b1b"
                        }
                    ]
                }
                , {
                    "featureType": "road"
                    , "elementType": "geometry.fill"
                    , "stylers": [
                        {
                            "color": "#2c2c2c"
                        }
                    ]
                }
                , {
                    "featureType": "road"
                    , "elementType": "labels.text.fill"
                    , "stylers": [
                        {
                            "color": "#8a8a8a"
                        }
                    ]
                }
                , {
                    "featureType": "road.arterial"
                    , "elementType": "geometry"
                    , "stylers": [
                        {
                            "color": "#373737"
                        }
                    ]
                }
                , {
                    "featureType": "road.highway"
                    , "elementType": "geometry"
                    , "stylers": [
                        {
                            "color": "#f2f200"
                        }
                        , {
                            "saturation": -70
                        }
                        , {
                            "lightness": 15
                        }
                        , {
                            "weight": 1.5
                        }
                    ]
                }
                , {
                    "featureType": "road.highway.controlled_access"
                    , "elementType": "geometry"
                    , "stylers": [
                        {
                            "color": "#4e4e4e"
                        }
                    ]
                }
                , {
                    "featureType": "road.local"
                    , "elementType": "labels.text.fill"
                    , "stylers": [
                        {
                            "color": "#616161"
                        }
                    ]
                }
                , {
                    "featureType": "transit"
                    , "elementType": "labels.text.fill"
                    , "stylers": [
                        {
                            "color": "#757575"
                        }
                    ]
                }
                , {
                    "featureType": "water"
                    , "elementType": "geometry"
                    , "stylers": [
                        {
                            "color": "#000000"
                        }
                    ]
                }
                , {
                    "featureType": "water"
                    , "elementType": "labels.text.fill"
                    , "stylers": [
                        {
                            "color": "#3d3d3d"
                        }
                    ]
                }
            ]
        });
        var marker = new google.maps.Marker({
            position: uluru
            , map: map
            , icon: root_url + '/public/images/puntero.png'
            ,
        });
        $(function () { // centrar solo si es version tablet o superior
            if ($('#btn-mobile').is(':hidden')) {
                var center = new google.maps.LatLng(21.14625074390243, -86.85021844108832);
                map.panTo(center);
            }
        });
    };
    var head = document.getElementsByTagName("head")[0];
    var maps = document.createElement("script");
    maps.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyDcFiBemCBysInrvP7zbzq8FBfU-0JUj8Y&callback=initMap";
    head.appendChild(maps);

    var captcha = document.createElement("script");
    captcha.src = "https://www.google.com/recaptcha/api.js";
    head.appendChild(captcha);
};

var showModal = function (e) {
    var esto = e.currentTarget;
    modalProyectos.modal("show");
    tituloProyectos.text(esto.title);
    modalImagen.attr("src", esto.id);
};


var scrollar = function () {
    if (navBar.offset().top > 90) {
        navBar.removeClass("navbar-transparent");
        btnUp.addClass("visible");
    } else {
        navBar.addClass("navbar-transparent");
        btnUp.removeClass("visible");
    }
};
var scrollAnimate = function (e) {
    e.preventDefault();
    var target = $(this.hash);
    var esto = this;
    $('html, body').stop().animate({
        'scrollTop': target.offset().top - 75
    }, 800, 'swing', function () {
        //window.location.hash = esto.hash;
    });
};

var validar = function (e) {
    e.preventDefault();
    grecaptcha.execute();
};

var onSubmit = function () {
    var params = formulario.serialize();
    $.post(root_url + "/email", params, function (data) {
        formulario.addClass("enviado").get(0).reset();
        dataLayer.push({ 'event': 'virtual_view_email' });
    }, "json");
};