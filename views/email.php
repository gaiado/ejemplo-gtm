<?php
require ROOT_DIR . '/vendor/phpmailer/phpmailer/PHPMailerAutoload.php';

$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : false;
$email = isset($_POST['email']) ? $_POST['email'] : false;
$telefono = isset($_POST['telefono']) ? $_POST['telefono'] : false;
$mensaje = isset($_POST['mensaje']) ? $_POST['mensaje'] : false;

ob_start();
?>

<table>
    <tr>
        <td>Nombre</td>
        <td><?php echo $nombre ?></td>
    </tr>
    <tr>
        <td>Email</td>
        <td><?php echo $email ?></td>
    </tr>
    <tr>
        <td>Telefono</td>
        <td><?php echo $telefono ?></td>
    </tr>
    <tr>
        <td>Mensaje</td>
        <td><?php echo $mensaje ?></td>
    </tr>
</table>

<?php
$html = ob_get_clean();

//Create a new PHPMailer instance
$mail = new PHPMailer;

$mail->CharSet = 'UTF-8';
// Set PHPMailer to use the sendmail transport
$mail->isSendmail();
//Set who the message is to be sent from
$mail->setFrom($email, 'Formulario XDA');
//Set an alternative reply-to address
$mail->addReplyTo($email, $nombre);
//Set who the message is to be sent to
$mail->addAddress('pootalejandro@yahoo.fr', 'Alejandro Poot');

$mail->Subject = 'Formulario XDA';

$mail->isHTML(true);

$mail->Body = $html;

$mail->send();

echo json_encode(array(
    'enviado' => true
));
