<!DOCTYPE html>
<html lang="es_MX"> 
    <head>
        <title>Agencia de Marketing en Cancún | XDA</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keywords" content="XDA, Diseño de Páginas Web Cancún, Marketing Digital en Cancún, Diseño Gráfico Cancún, XDA, Edición de videos en Cancún, Fotografía, Branding, XDA">
        <meta name="description" content="XDA, Expertos en marketing digital, diseño web y diseño gráfico. Agencia creativa en Cancún, XDA">
        <meta name="robots" content="index, follow">
        <link rel="icon" href="<?php echo ROOT_URL ?>/public/images/favicon.png" type="image/png" sizes="16x16">
        <style>
            body .btn-up{width:40px;height:40px;background-color:rgba(34, 34, 32, 0.64);color:white;border:2px solid #E8B33B;-webkit-box-shadow:rgba(0, 0, 0, 0.32) 0px 2px 4px 0px;box-shadow:rgba(0, 0, 0, 0.32) 0px 2px 4px 0px;font-size:32px;position:fixed;bottom:50px;right:100px;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;text-decoration:none!important;opacity:0;-webkit-transition-duration:.3s;-o-transition-duration:.3s;transition-duration:.3s}body .btn-up.visible{opacity:1}body .btn{border-radius:0}body .form-control{border-radius:0}body .btn-amarillo{border:#E8B33B solid 2px;color:#E8B33B;-webkit-transition-duration:0.3s;-o-transition-duration:0.3s;transition-duration:0.3s}body .btn-amarillo:hover{color:#FFF;background-color:#E8B33B}@media (min-width:768px){body .navbar-fixed-top.navbar-transparent{background-color:transparent;border:none;opacity:1}}body{font-family:'Open Sans', sans-serif}body .navbar-inverse{background-color:rgba(34, 34, 32, 0.9)}body .navbar-inverse .navbar-nav>li{position:relative}body .navbar-inverse .navbar-nav>li:after{position:absolute;content:"";background-color:#E8B33B;height:2px;left:15px;right:15px;bottom:15px;opacity:0;-webkit-transition-duration:.3s;-o-transition-duration:.3s;transition-duration:.3s}body .navbar-inverse .navbar-nav>li.active:after{opacity:1}body .navbar-inverse .navbar-nav>li.active a{background:none}body .navbar-inverse .navbar-nav>li.social-links>a{display:inline-block;font-size:16px}body .navbar-inverse .navbar-nav>li.social-links>a .fa.fa-instagram{border-radius:12px 12px 12px 12px}body .navbar-inverse .navbar-nav>li.social-links>a.active .fa,body .navbar-inverse .navbar-nav>li.social-links>a:hover .fa{-webkit-transition-duration:.3s;-o-transition-duration:.3s;transition-duration:.3s}body .navbar-inverse .navbar-nav>li.social-links>a.active .fa.fa-facebook-f,body .navbar-inverse .navbar-nav>li.social-links>a:hover .fa.fa-facebook-f{color:#3b5999}body .navbar-inverse .navbar-nav>li.social-links>a.active .fa.fa-twitter,body .navbar-inverse .navbar-nav>li.social-links>a:hover .fa.fa-twitter{color:#55acee}body .navbar-inverse .navbar-nav>li.social-links>a.active .fa.fa-youtube,body .navbar-inverse .navbar-nav>li.social-links>a:hover .fa.fa-youtube{color:#e52d27}body .navbar-inverse .navbar-nav>li.social-links>a.active .fa.fa-instagram,body .navbar-inverse .navbar-nav>li.social-links>a:hover .fa.fa-instagram{background:-webkit-linear-gradient(45deg, #f9ed32 0%, #ef3d73 90%, #ee2a7b 100%);background:-o-linear-gradient(45deg, #f9ed32 0%, #ef3d73 90%, #ee2a7b 100%);background:linear-gradient(45deg, #f9ed32 0%, #ef3d73 90%, #ee2a7b 100%)}body .navbar-fixed-top{-webkit-transition-duration:.3s;-o-transition-duration:.3s;transition-duration:.3s;border:none}body .navbar-fixed-top .navbar-nav>li>a{color:#FFF}body .navbar-brand{padding:0px}body .navbar-brand>img{height:100%;padding:15px;width:auto}.section-1{background-image:url(<?php echo ROOT_URL ?>/public/images/header2.jpg);background-position:center center;background-repeat:no-repeat;background-size:cover;min-height:796px;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;color:#FFF;position:relative}.section-1:before{content:"";position:absolute;width:100%;height:100%;top:0;left:0;background-color:rgba(72, 54, 12, 0.63)}.section-1>div{text-align:center;position:relative}.section-1>div h1{font-size:50px}.section-1>div p{font-size:25px;font-style:italic}@media (max-width:1191px){.section-1{min-height:574px}.section-1>div h1{font-size:42px}.section-1>div p{font-size:22px}}@media (max-width:767px){.section-1{min-height:322px}}.section-2{min-height:122px;padding-bottom:15px;padding-top:15px;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;font-size:20px}.section-2>div{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex}.section-2>div .list-inline li+li:before{content:"|";padding-right:15px}.section-2>div .btn{font-size:inherit}@media (max-width:1191px){.section-2{min-height:88px;font-size:16px}}@media (max-width:767px){.section-2{text-align:center;min-height:194px}.section-2>div{-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column}}
            body .btn-up{width:40px;height:40px;background-color:rgba(34, 34, 32, 0.64);color:white;border:2px solid #E8B33B;-webkit-box-shadow:rgba(0, 0, 0, 0.32) 0px 2px 4px 0px;box-shadow:rgba(0, 0, 0, 0.32) 0px 2px 4px 0px;font-size:32px;position:fixed;bottom:50px;right:100px;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;text-decoration:none!important;opacity:0;-webkit-transition-duration:.3s;-o-transition-duration:.3s;transition-duration:.3s}body .btn-up.visible{opacity:1}body .btn{border-radius:0}body .form-control{border-radius:0}body .btn-amarillo{border:#E8B33B solid 2px;color:#E8B33B;-webkit-transition-duration:0.3s;-o-transition-duration:0.3s;transition-duration:0.3s}body .btn-amarillo:hover{color:#FFF;background-color:#E8B33B}@media (min-width:768px){body .navbar-fixed-top.navbar-transparent{background-color:transparent;border:none;opacity:1}}body{font-family:'Open Sans', sans-serif}body .navbar-inverse{background-color:rgba(34, 34, 32, 0.9)}body .navbar-inverse .navbar-nav>li{position:relative}body .navbar-inverse .navbar-nav>li:after{position:absolute;content:"";background-color:#E8B33B;height:2px;left:15px;right:15px;bottom:15px;opacity:0;-webkit-transition-duration:.3s;-o-transition-duration:.3s;transition-duration:.3s}body .navbar-inverse .navbar-nav>li.active:after{opacity:1}body .navbar-inverse .navbar-nav>li.active a{background:none}body .navbar-inverse .navbar-nav>li.social-links>a{display:inline-block;font-size:16px}body .navbar-inverse .navbar-nav>li.social-links>a .fa.fa-instagram{border-radius:12px 12px 12px 12px}body .navbar-inverse .navbar-nav>li.social-links>a.active .fa,body .navbar-inverse .navbar-nav>li.social-links>a:hover .fa{-webkit-transition-duration:.3s;-o-transition-duration:.3s;transition-duration:.3s}body .navbar-inverse .navbar-nav>li.social-links>a.active .fa.fa-facebook-f,body .navbar-inverse .navbar-nav>li.social-links>a:hover .fa.fa-facebook-f{color:#3b5999}body .navbar-inverse .navbar-nav>li.social-links>a.active .fa.fa-twitter,body .navbar-inverse .navbar-nav>li.social-links>a:hover .fa.fa-twitter{color:#55acee}body .navbar-inverse .navbar-nav>li.social-links>a.active .fa.fa-youtube,body .navbar-inverse .navbar-nav>li.social-links>a:hover .fa.fa-youtube{color:#e52d27}body .navbar-inverse .navbar-nav>li.social-links>a.active .fa.fa-instagram,body .navbar-inverse .navbar-nav>li.social-links>a:hover .fa.fa-instagram{background:-webkit-linear-gradient(45deg, #f9ed32 0%, #ef3d73 90%, #ee2a7b 100%);background:-o-linear-gradient(45deg, #f9ed32 0%, #ef3d73 90%, #ee2a7b 100%);background:linear-gradient(45deg, #f9ed32 0%, #ef3d73 90%, #ee2a7b 100%)}body .navbar-fixed-top{-webkit-transition-duration:.3s;-o-transition-duration:.3s;transition-duration:.3s;border:none}body .navbar-fixed-top .navbar-nav>li>a{color:#FFF}body .navbar-brand{padding:0px}body .navbar-brand>img{height:100%;padding:15px;width:auto}.section-1{background-image:url(<?php echo ROOT_URL ?>/public/images/header2.jpg);background-position:center center;background-repeat:no-repeat;background-size:cover;min-height:796px;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;color:#FFF;position:relative}.section-1:before{content:"";position:absolute;width:100%;height:100%;top:0;left:0;background-color:rgba(72, 54, 12, 0.63)}.section-1>div{text-align:center;position:relative}.section-1>div h1{font-size:50px}.section-1>div p{font-size:25px;font-style:italic}@media (max-width:1191px){.section-1{min-height:574px}.section-1>div h1{font-size:42px}.section-1>div p{font-size:22px}}@media (max-width:767px){.section-1{min-height:322px}}.section-2{min-height:122px;padding-bottom:15px;padding-top:15px;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;font-size:20px}.section-2>div{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex}.section-2>div .list-inline li+li:before{content:"|";padding-right:15px}.section-2>div .btn{font-size:inherit}@media (max-width:1191px){.section-2{min-height:88px;font-size:16px}}@media (max-width:767px){.section-2{text-align:center;min-height:194px}.section-2>div{-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column}}.modal-title{color: #000;}
        </style>  
        <script>
            root_url = '<?php echo ROOT_URL ?>';
            dataLayer=[];
        </script>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MQP9QNJ');</script>
        <!-- End Google Tag Manager -->
    </head>

    <body data-spy="scroll" data-target=".navbar" data-offset="75">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MQP9QNJ"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
        <nav class="navbar navbar-inverse navbar-fixed-top navbar-transparent">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button id="btn-mobile" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button> <a class="navbar-brand scrolll-animate" href="#home">
                        <img class="img-responsive" src="<?php echo ROOT_URL ?>/public/images/logo.png" alt="Logo XDA" title="Logo XDA">
                    </a> </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="hidden">
                            <a class="scrolll-animate" href="#home"></a>
                        </li>
                        <li>
                            <a title="Nosotros XDA" class="scrolll-animate" href="#nosotros">
                                <?php echo $i18n['menu_nosotros'] ?>
                            </a>
                        </li>
                        <li>
                            <a title="Servicios XDA" class="scrolll-animate" href="#servicios">
                                <?php echo $i18n['menu_servicios'] ?>
                            </a>
                        </li>
                        <li>
                            <a title="Clientes XDA" class="scrolll-animate" href="#clientes">
                                <?php echo $i18n['menu_clientes'] ?>
                            </a>
                        </li>
                        <li>
                            <a title="Contacto XDA" class="scrolll-animate" href="#contacto">
                                <?php echo $i18n['menu_contacto'] ?>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="social-links"> <a title="Facebook XDA" target="_blank" href="https://www.facebook.com/"><span class="fa fa-facebook-f"></span></a> <a title="Instagram XDA" target="_blank" href="https://www.instagram.com/"><span class="fa fa-instagram"></span></a> <a title="Twitter XDA" target="_blank" href="https://twitter.com/"><span class="fa fa-twitter"></span></a> <a title="YouTube XDA" target="_blank" href="https://www.youtube.com/"><span class="fa fa-youtube"></span></a> </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
        <section class="section-1" id="home">
            <div>
                <h1>XDA</h1>
                <p>Convirtiendo tus ideas en éxitos</p>
            </div>
        </section>
        <section class="section-2">
            <div class="container">
                <div>
                    <ul class="list-inline">
                        <li>Estrategia digital</li>
                        <li>Diseño gráfico</li>
                        <li>Páginas web</li>
                        <li>Redes sociales</li>
                        <li>Aplicaciones móviles</li>
                        <li>Fotografía</li>
                        <li>Branding</li>
                        <li>Identidad corporativa</li>
                        <li>Analítica web</li>
                        <li>Registro de marca</li>
                    </ul>
                </div>
                <div> <a href="#contacto" title="Agenda una cita" class="btn btn-amarillo scrolll-animate">Agenda una cita</a> </div>
            </div>
        </section>
        <section class="section-3" id="nosotros">
            <div class="container">
                <h2>Somos <strong>XDA</strong></h2> <img alt="XDA rombo" class="wow fadeInDown" src="<?php echo ROOT_URL ?>/public/images/rombo.png"> 
                <p>Somos una <strong>agencia de marketing digital en Cancún</strong> que, gracias a la combinación de estrategias en fotografía, video, diseño gráfico y gestión, logramos los objetivos de cada uno de nuestros clientes en el corto, mediano y largo plazo, generando tiempo valioso para que los dueños y los departamentos directivos puedan dedicarse a darle dirección a su negocio, somos XDA.</p>
            </div>
        </section>
        <section class="section-4" id="servicios">
            <header>
                <h2>Servicios y estrategias</h2> </header>
            <div>
                <div class="bg bg-1">
                    <div class="animation"> <img src="<?php echo ROOT_URL ?>/public/images/social/1.png" alt="Gestión de redes sociales" class="img-responsive wow rotateIn"> <img src="<?php echo ROOT_URL ?>/public/images/social/2.png" alt="Benchmarking strategy" class="img-responsive wow tada"> <img src="<?php echo ROOT_URL ?>/public/images/social/3.png" alt="Publicaciones estratégicas" class="img-responsive wow zoomIn"> </div>
                </div>
                <div>
                    <h3>Estrategia digital</h3> <strong>Gestión de redes  sociales</strong>
                    <p>Aprovechamos las herramientas que las redes nos otorgan para generar estrategias precisas para el cumplimiento de los objetivos de tu marca.</p> <strong>Benchmarking strategy</strong>
                    <p>El análisis de la competencia es parte fundamental de una estrategia exitosa, logramos aprovechar las tendencias, errores y aciertos de nuestros competidores directos e indirectos.</p> <strong>Publicaciones estratégicas </strong>
                    <p>Las publicaciones son analizadas antes, durante y después de llegar al mercado meta, aumentando la interacción de tu marca. </p>
                </div>
                <div>
                    <h3>Desarrollo web y mobile</h3> <strong>Diseño y  programación web </strong>
                    <p>Sitios web responsive, dinámicos y altamente interactivos. Hacemos que tu imagen web sea atractiva y ajustable a diferentes dispositivos.</p> <strong>Comercio electronico</strong>
                    <p>Amplia tus horizontes. Te ayudamos a generar una estrategia para virtualizar tu negocio y llegues a más clientes.</p> <strong>Diseño y  programación mobile </strong>
                    <p>Diseñamos aplicaciones de acuerdo a la necesidad de tu negocio, usamos las mejores tecnologías del mercado y combinadas con estrategias de marketing.</p>
                </div>
                <div class="bg bg-2">
                    <div class="animation"> <img src="<?php echo ROOT_URL ?>/public/images/desarrollo/1.png" alt="Diseño y programación web" class="img-responsive wow rotateIn"> <img src="<?php echo ROOT_URL ?>/public/images/desarrollo/2.png" alt="Comercio electronico" class="img-responsive wow tada"> <img src="<?php echo ROOT_URL ?>/public/images/desarrollo/3.png" alt="Diseño y programación mobile" class="img-responsive wow bounce"> </div>
                </div>
                <div class="bg bg-3">
                    <div class="animation"> <img src="<?php echo ROOT_URL ?>/public/images/diseno/1.png" alt="Imagen y diseño creativo" class="img-responsive wow rotateIn"> <img src="<?php echo ROOT_URL ?>/public/images/diseno/2.png" alt="Registro de marca" class="img-responsive wow tada"> <img src="<?php echo ROOT_URL ?>/public/images/diseno/3.png" alt="Diseño y programación mobile" class="img-responsive wow flip"> <img src="<?php echo ROOT_URL ?>/public/images/diseno/4.png" alt="Identidad corporativa" class="img-responsive wow fadeInUp"> </div>
                </div>
                <div>
                    <h3>Imagen y diseño creativo</h3> <strong>Registro de marca</strong>
                    <p>Trabajamos con expertos a través de una alianza estratégica que garantiza el registro de tu proyecto como propiedad intelectual o derechos de autor.</p> <strong>Diseño gráfico online y offline</strong>
                    <p>La creación de las ideas se plasma de manera gráfica en cualquier proyecto, desde la creación de unas tarjetas de presentación, menús, logo, publicidad para redes, etc.</p> <strong>Identidad corporativa </strong>
                    <p>Parte elemental para la creación de tu marca, congeniamos la identidad gráfica con los objetivos de la empresa logrando una sinergia en la comunicación interna y externa.</p>
                </div>
            </div>
        </section>
        <section class="section-5" id="clientes">
            <header>
                <h2>Portafolio</h2> <img alt="Rombo XDA" class="wow fadeInDown" src="<?php echo ROOT_URL ?>/public/images/rombo.png">
                <p>Nuestra creatividad no tiene límite, te invitamos a ser nuestro siguiente caso de éxito en <strong>XDA</strong></p>
            </header>
            <div class="list-proyectos"> <a class="item-proyecto" title="AVIVA" id="<?php echo ROOT_URL ?>/public/images/mockups/aviva.jpg">
                    <img alt="AVIVA cliente XDA" class="img-responsive" src="<?php echo ROOT_URL ?>/public/images/porfolio/aviva.jpg">
                    <div class="titulo-proyecto">
                        <div>
                            AVIVA
                            <div><i class="fa fa-eye fa-2x" aria-hidden="true"></i></div>
                        </div>                        
                    </div>
                </a> <a class="item-proyecto" title="Cervecería Tulum" id="<?php echo ROOT_URL ?>/public/images/mockups/cerveceria-tulum.jpg">
                    <img alt="Cervecería Tulum cliente XDA" class="img-responsive" src="<?php echo ROOT_URL ?>/public/images/porfolio/tulum.jpg">
                    <div class="titulo-proyecto">
                        <div>
                            Cervecería Tulum
                            <div><i class="fa fa-eye fa-2x" aria-hidden="true"></i></div>
                        </div>                        
                    </div>
                </a> <a class="item-proyecto" title="Puerto Santo" id="<?php echo ROOT_URL ?>/public/images/mockups/puerto-santo.jpg">
                    <img alt="Puerto Santo cliente XDA" class="img-responsive" src="<?php echo ROOT_URL ?>/public/images/porfolio/puerto-santo.jpg">
                    <div class="titulo-proyecto">
                        <div>
                            Puerto Santo
                            <div><i class="fa fa-eye fa-2x" aria-hidden="true"></i></div>
                        </div>                        
                    </div>
                </a> <a class="item-proyecto" title="Siembra Educación Financiera" id="<?php echo ROOT_URL ?>/public/images/mockups/sef.jpg">
                    <img alt="Siembra Educación Financiera" class="img-responsive" src="<?php echo ROOT_URL ?>/public/images/porfolio/siembra-educacion-financiera.jpg">
                    <div class="titulo-proyecto">
                        <div>
                            Siembra Educación Financiera
                            <div><i class="fa fa-eye fa-2x" aria-hidden="true"></i></div>
                        </div>                        
                    </div>
                </a> </div>
                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title" id="myModalLabel">Modal title</h3> </div>
                            <div class="modal-body"> <img class="img-responsive" id="modal-imagen" alt="Clientes XDA" title="Clientes XdA"> </div>
                        </div>
                    </div>
                </div>
            <!-- Modal -->
        </section>
        <section class="section-6">
            <div id="mapa"></div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-5">
                        <form id="contacto" method="post">
                            <h2>Contacto</h2>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Nombre" required name="nombre"> </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email" required name="email"> </div>
                            <div class="form-group">
                                <input type="tel" class="form-control" placeholder="Telefono" required name="telefono"> </div>
                            <div class="form-group">
                                <textarea rows="6" class="form-control" placeholder="Mensaje" required name="mensaje"></textarea>
                            </div>
                            <button type="submit" class="btn btn-warning btn-block">ENVIAR</button>
                            <div class="g-recaptcha"
                                 data-sitekey="6LfBGR8UAAAAAKG3NqvmtIp1q_VMxhbNvIc4c2U_"
                                 data-callback="onSubmit"
                                 data-size="invisible"
                                 data-badge="bottomleft">
                            </div>
                            <p class="mensaje"> MENSAJE ENVIADO </p>
                        </form>
                        <div class="direccion">
                            <p><span class="fa fa-home"></span> Av del Bosque Lote 22, Colonia Pedregal del bosque SM 43 CP 77506 Cancún Quintana Roo</p>
                            <p><span class="fa fa-phone"></span> <a href="tel:9982514930">998 2514930</a> </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <a class="scrolll-animate btn-up" id="btn-up" href="#home"> <i class="fa fa-angle-up" aria-hidden="true"></i> </a>

        <link href="<?php echo ROOT_URL ?>/public/css/style.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo ROOT_URL ?>/public/bower_components/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo ROOT_URL ?>/public/bower_components/wow/dist/wow.min.js"></script>
        <script type="text/javascript" src="<?php echo ROOT_URL ?>/public/bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js"></script>
        <link rel="stylesheet" href="<?php echo ROOT_URL ?>/public/bower_components/animate.css/animate.min.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700" />
        <script src="<?php echo ROOT_URL ?>/public/js/script-dist.js"></script>  
    </body>

</html>